const mongoose = require("mongoose");
const Product = require("../models/products.js");

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		createdOn: reqBody.createdOn
	})
	
	return newProduct.save().then((newProduct, error) => {
		if(error){
			return error;
		}
		else{
			return newProduct;
		}
	})
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
			return result;
	})
}

// GET specific course
module.exports.getProduct = (courseId) => {

						//inside the parenthesis should be the id
	return Product.findById(courseId).then(result => {
		return result;
	})
}
										// it will contain multiple fields
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Product.findByIdAndUpdate(productId , 
			{			//req.body  
				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price,
				createdOn: reqBody.createdOn
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

